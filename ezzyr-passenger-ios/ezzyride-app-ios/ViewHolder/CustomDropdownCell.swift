//
//  CustomDropdownCell.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 12/3/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class CustomDropdownCell: DropDownCell {
    
    @IBOutlet weak var locationImageView: UIImageView!
    
    @IBOutlet weak var formattedAddress: UILabel!
}
