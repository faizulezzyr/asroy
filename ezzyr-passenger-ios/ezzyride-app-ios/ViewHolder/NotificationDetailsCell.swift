//
//  NotificationDetailsCell.swift
//  ezzyride-app-ios
//
//  Created by Md. Rahman Rahman on 7/1/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class NotificationDetailsCell: UITableViewCell {

    @IBOutlet weak var callView: UIView!
    @IBOutlet weak var detailsText: UILabel!
    
    @IBOutlet weak var customCallButton: UILabel!
}





