//
//  NotificationInfoCell.swift
//  ezzyride-app-ios
//
//  Created by Md. Rahman Rahman on 6/1/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class NotificationInfoCell: UITableViewCell {

    @IBOutlet weak var category: UILabel!
    
    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var subtitle: UILabel!
    
    @IBOutlet weak var customImageView: UIImageView!
}
