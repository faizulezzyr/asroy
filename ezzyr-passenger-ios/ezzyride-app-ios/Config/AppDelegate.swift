//
//  AppDelegate.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/22/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces
import Firebase
import UserNotifications
import FirebaseMessaging
import SwiftyJSON
import PubNub
import PullUpController


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,PNObjectEventListener {
    //
    //    var client : PubNub
    //    var config : PNConfiguration
    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {       
        IQKeyboardManager.shared.enable = true
        DropDown.startListeningToKeyboard()
        UIApplication.shared.statusBarStyle = .lightContent
        
        GMSServices.provideAPIKey(Constants.GOOGLE_MAP_API_KEY)
        GMSPlacesClient.provideAPIKey(Constants.GOOGLE_MAP_API_KEY)
        
        FirebaseApp.configure()
        doRegisterForRemoteNotification(application)
        
        self.window!.rootViewController = initViewController()
        self.window?.makeKeyAndVisible()
        return true
    }
    
    
    private func initViewController() -> UINavigationController {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let splashController = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [splashController!]
        return navigationController
    }
    
    func doRegisterForRemoteNotification(_ application: UIApplication) {
        
        Messaging.messaging().delegate = self
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(" Rcv remote\(userInfo)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        let content = UNMutableNotificationContent()
        if #available(iOS 10.0, *)
        {
            print(userInfo)
            if let info = userInfo as? [String: Any] {
                if let msg = info["message"] as? String {
                    
                 NotificationCenter.default.post(name: .pushPayload, object: nil, userInfo: userInfo)
                    let message = JSON.init(parseJSON: msg)
                    print(message)
                    if let action = message["action"].string {
                        msgAction = action
                        if action == "accept_trip_request" {
                          content.body = "Request accepted by the Police"
                            if let submsg = message["msg"].dictionary {
                                if let requestId = submsg["request_id"]?.int{
                                    Helpers.setIntValueWithKey(requestId, key: Constants.TRIP_ID)
                              
                                }
                            }
                            
                            
                        }else if action == "ready_to_pickup" {
                          content.body = "Rescue Completed"
                         
                        }else if action == "trip_start" {
                         content.body = "Started the trip"
                        
                        }else if action == "end_trip"{
                         content.body = "Ended the trip"
                         
                        }else if action == "new_content_notification" {
                            if let submsg = message["msg"].dictionary {
                                if let msgTitle = submsg["headline"]?.string {
                                   content.title = msgTitle
                                }
                               
                                if let headline = submsg["subhead"]?.string{
                                    content.body = headline
                                }
//                                if let requestId = submsg["request_id"]?.string{
//                                    Helpers.setStringValueWithKey(requestId, key: Constants.TRIP_ID)
//                                }
                            }
                        }else if action == "cancel_trip_request" {
                           content.body = "Trip canceled by the driver"
                        }else if action == "driver_appply" {
                            content.body = "driver appply on your booking"
                        }else if action == "trip_pause"{
                            content.body = "Ezzyr driver has marked you, safely reached in distination. Please confirm us"
                        }else if action == "trip_resume"{
                            content.body = "Return trip started"
                        }
                       
                    }
                }
            }
            
            let request = UNNotificationRequest(identifier: "none", content: content, trigger: nil)
            UNUserNotificationCenter.current().add(request)
            {
                error in // called when message has been sent
                debugPrint("Error: \(String(describing: error))")
            }
            
 
        }
        
        completionHandler(UIBackgroundFetchResult.newData)
    }
    
    // [END receive_message]
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        // With swizzling disabled you must set the APNs token here.
        Messaging.messaging().apnsToken = deviceToken
        
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

//    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
//        if let components = NSURLComponents(URL: url, resolvingAgainstBaseURL: true), let path = components.path, let query = components.query {
//            if path == "/profile" {
//                // Pass the profile ID from the URL to the view controller.
//                return profileViewController.loadProfile(query)
//            }
//        }
//        return false
//    }
}

@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        
        if #available(iOS 10.0, *)
        {

            NotificationCenter.default.post(name: .pushPayload, object: nil, userInfo: userInfo)
        }
        
        completionHandler([.alert, .sound, .badge])
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                didReceive response: UNNotificationResponse,
                                withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)
        completionHandler()
        if #available(iOS 10.0, *)
        {
        NotificationCenter.default.post(name: .pushPayload, object: nil, userInfo: userInfo)
        }
    }
}

extension AppDelegate: MessagingDelegate {
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        Helpers.removeValue(Constants.FCM_TOKEN)
        Helpers.setStringValueWithKey(fcmToken, key: Constants.FCM_TOKEN)
        
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    }
    
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("Received data message: \(remoteMessage.appData)")
    }
}









