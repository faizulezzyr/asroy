//
//  ResetPasswordController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 18/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ResetPasswordController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var sendNumberMsg: UILabel!
    @IBOutlet weak var code1: UITextField!
    @IBOutlet weak var code2: UITextField!
    @IBOutlet weak var code3: UITextField!
    @IBOutlet weak var code4: UITextField!
    @IBOutlet weak var code5: UITextField!
    @IBOutlet weak var timeCountmsg: UILabel!
    @IBOutlet weak var resendCode: UIButton!
    @IBOutlet weak var newPassword: UITextField!
    @IBOutlet weak var confirmpass: UIButton!
    
    var authenticationService: AuthenticationService?
    var apiCommunicatorHelper: APICommunicator?
    var buttonTappdValue = ""
    var count = 50
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        code1.delegate = self
        code2.delegate = self
        code3.delegate = self
        code4.delegate = self
        code5.delegate = self
        initApiCommunicatorHelper()
        uiChange()
    }
    
    func uiChange(){
        Commons.bottomCornerRedious(button: confirmpass, cornerRedious: 20)
        Commons().placeHolderColorChange(TextFild: newPassword, PlaceHolder: "New password")
        self.sendNumberMsg.text = "Enter the 5-digit code sent to you at \(numberForForgotPassord)"
        _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
        self.resendCode.isUserInteractionEnabled = false
    }
    
    @objc func update() {
        if(count > 0) {
            count -= 1
            timeCountmsg.text = "Resend code in \(count)"
            
        }
        if count == 0 {
            resendCode.isUserInteractionEnabled = true
            timeCountmsg.isHidden = true
        }
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    ///For taking verification code manually
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! < 1  && string.count > 0){
            if(textField == code1){
                code1.text = buttonTappdValue
                code2.becomeFirstResponder()
            }
            if(textField == code2){
                code3.becomeFirstResponder()
            }
            if(textField == code3){
                code4.becomeFirstResponder()
            }
            if (textField == code4){
                code5.becomeFirstResponder()
            }
            textField.text = string
            return false
            
        }else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField == code2){
                code2.becomeFirstResponder()
            }
            if(textField == code3){
                code2.becomeFirstResponder()
            }
            if(textField == code4) {
                code3.becomeFirstResponder()
            }
            if (textField == code5){
                code4.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }else if ((textField.text?.count)! >= 1  ){
            textField.text = string
            return false
        }
        return true
    }
    
    
    func verifyCode(){
        let code1 = self.code1.text ?? ""
        let code2 = self.code2.text ?? ""
        let code3 = self.code3.text ?? ""
        let code4 = self.code4.text ?? ""
        let code5 = self.code5.text ?? ""
        let password = newPassword.text!
        if code1.count > 0 && code2.count > 0 && code3.count > 0 && code4.count > 0 && code5.count > 0 {
            let bindAuthCode = code1 + code2 + code3 + code4 + code5
            doValidateCode(bindAuthCode, password)
        }
        else {
            let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.MISSING_MANDATORY_FIELD)
            self.present(popup, animated: true, completion: nil)
        }
    }
    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    
    @IBAction func confirm(_ sender: Any) {
        verifyCode()
    }
    
    @IBAction func resend(_ sender: Any) {
        
    }
    //MARK: Api Implementation for send OTP
    func doValidatePhoneNumber(_ mobileNumber: String) {
        
        let params = [
            "mobile_no": numberForForgotPassord,
            "user_type": String(UserType.One.rawValue)
            
        ]
        authenticationService?.doForgotPasswordOtpRequest(params)
    }
    
    @IBAction func back(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    
    
    func getValidatePhoneNumberResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            var success = false
            if !response.isEmpty {
                if let code = response["code"].string {
                    if Int(code) == Constants.STATUS_CODE_SUCCESS {
                        if let loginStatus = response["success"].bool {
                            success = loginStatus
                            if success == true {
                                
                                let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.CREATE_SUCCESS_MSG)
                                self.present(popup, animated: true, completion: nil)
                                
                            }else {
                                if let reson = response["message"].string {
                                    let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                                    self.present(popup, animated: true, completion: nil)
                                }
                                
                            }
                        }
                    }
                    else {
                        //need to handle
                    }
                }
            }
        }
        else {
            //need to handle
        }
    }
    
    
    //MARK: API Implementation for reset password
    func doValidateCode(_ code: String, _ newPassword: String){
        if newPassword.count > 5 {
            let params = [
                
                "new_password": newPassword,
                "token": code
                
            ]
            authenticationService?.doResetPassword(params)
        }else{
            let popup = CustomAlertPopup.customOkayPopup("", msgBody: "Password must be 6")
            self.present(popup, animated: true, completion: nil)
        }

    }
    
    func getValideCodeResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            if !response.isEmpty {
                if let success = response["success"].bool {
                    if success {
                        if let token = response["token"].string {
                            Helpers.removeValue(Constants.ACCESS_TOKEN)
                            Helpers.setStringValueWithKey(token, key: Constants.ACCESS_TOKEN)
                        }
                        navaigationToHome()
                    }
                    else {
                        //msg Invalid Code
                        let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.INVALID_AUTH_CODE)
                        self.present(popup, animated: true, completion: nil)
                    }
                }
            }
        }
        else {
            //need to handle
        }
    }
}




extension ResetPasswordController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getValideCodeResponse(data, statusCode: statusCode)
        }else if methodTag == MethodTags.THIRD {
            getValidatePhoneNumberResponse(data, statusCode: statusCode)
        }
    }
}







