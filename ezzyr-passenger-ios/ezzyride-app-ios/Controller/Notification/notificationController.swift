import UIKit
import SwiftyJSON
import Kingfisher
import LSDialogViewController

var webViewUrl = ""
class notificationController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var section = 0
    
    @objc func callButtonPressed(_ sender: UITapGestureRecognizer) {
        if let callButtonLabel = sender.view as? UILabel {
            print(callButtonLabel.tag)
            if let number = messageArray[callButtonLabel.tag]["action_value"].string {
                if let textAction = messageArray[callButtonLabel.tag]["action"].string {
                    if textAction == "CALL"{
                      number.doMakeAPhoneCall()
                    }else{
                       webViewUrl = number
                       goWebView()
                    }
                }
               
            }
            
        }
    }
    
    func goWebView(){
      performSegue(withIdentifier: "detailsViewNoti", sender: self)
       
     
    }
    @IBOutlet weak var tableView: UITableView!
    var userService: UserService?
    var apiCommunicatorHelper: APICommunicator?
    var messageArray: [JSON] = []
       let dialogViewController = ErrorView(nibName: "serverErrorMsg", bundle: nil)
    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
        initCustomTableView()
        dialogViewController.notifationDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgainNoti), name: NSNotification.Name(rawValue: "serverError"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgainNoti), name: NSNotification.Name(rawValue: "noInternet"), object: nil)
        getNotification()
        // Do any additional setup after loading the view.
    }
    @objc func tryAgainNoti(notif: NSNotification) {
        showInernetDialog(.slideBottomTop)
    }
    
    func showInernetDialog(_ animationPattern: LSAnimationPattern) {
        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
        globalViewControllerString = "notificationController"
    }
    func dismissInternetDialog() {
        dismissDialogViewController(LSAnimationPattern.fadeInOut)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.30) {
            self.getNotification()
        }
    }
    func getNotification(){
        userService!.getNotification()
    }
    func initCustomTableView () {
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        //        self.tableView.rowHeight = UITableView.automaticDimension
        //        self.tableView.estimatedRowHeight = 290
        
        let xib = UINib(nibName: "NotificationInfoCell", bundle: nil)
        self.tableView.register(xib, forCellReuseIdentifier: "Cell")
        
        let xib1 = UINib(nibName: "NotificationDetailsCell", bundle: nil)
        self.tableView.register(xib1, forCellReuseIdentifier: "Cell1")
    }
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        userService = UserService(self.view, communicator: apiCommunicatorHelper!)
        
    }
    func getMessageResponds(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            //print(response)
            if !response.isEmpty {
                if let code = response["code"].string {
                    if code == String(Constants.STATUS_CODE_SUCCESS) {
                        if let dataList = response["data"].array {
                            messageArray = dataList
                            self.tableView.reloadData()
                            
                        }
                    }
                }
            }else{
                
            }
        }
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return messageArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 400
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 400
        }
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if messageArray.count > 0 {
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! NotificationInfoCell
                if let cetagory = messageArray[indexPath.section]["category"].string {
                    cell.category.text = cetagory
                }
                
                if let title = messageArray[indexPath.section]["headline"].string {
                    cell.title.text = title
                }
                if let subTitle = messageArray[indexPath.section]["subhead"].string {
                    cell.subtitle.text = subTitle
                }
                
                if let image = messageArray[indexPath.section]["image_url"].string {
                    let imageURLString = image.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
                    let url = URL(string: imageURLString!)
                    cell.customImageView.contentMode = .scaleToFill
                    cell.customImageView.kf.indicatorType = .activity
                    KingfisherManager.shared.retrieveImage(with: url!, options: nil, progressBlock: nil, completionHandler: { image, error, cacheType, imageURL in
                        
                        cell.customImageView.image = image
                    })
                }
                return cell
            }
            else if indexPath.row == 1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell1", for: indexPath) as! NotificationDetailsCell

            
                if let description = messageArray[indexPath.section]["content"].string {
                    let htmlString = description.convertHTML()
                    cell.detailsText.font = UIFont.systemFont(ofSize: 20)
                    cell.detailsText.attributedText = htmlString
                }
      
                
                Commons.makeCardView(view: cell.callView)
                 if let textAction = messageArray[indexPath.section]["action"].string {
               
                       cell.customCallButton.text = textAction
                
                    
                }
             
                cell.customCallButton.tag = indexPath.section
                section = indexPath.section
                let tap = UITapGestureRecognizer(target: self, action: #selector(notificationController.callButtonPressed(_:)))
                cell.customCallButton.isUserInteractionEnabled = true
                cell.customCallButton.addGestureRecognizer(tap)
                return cell
            }
        }
        return UITableViewCell()
    }


    
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension notificationController: APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if  methodTag == MethodTags.FIRST{
            getMessageResponds(data, statusCode: statusCode)
        }
    }
}
