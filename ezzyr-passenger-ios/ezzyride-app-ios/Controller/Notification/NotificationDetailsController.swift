//
//  NotificationDetailsController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 21/5/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class NotificationDetailsController: UIViewController,UIWebViewDelegate {
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var indicatior: UIActivityIndicatorView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView.delegate = self
        indicatior.startAnimating()
        webView.loadRequest(URLRequest(url: URL(string: webViewUrl)!))
        // Do any additional setup after loading the view.
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        if webView.isLoading{
            return
        }
          indicatior.stopAnimating()
        
    }

    @IBAction func Back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    


}
