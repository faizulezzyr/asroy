//
//  notificationCell.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 3/1/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit

class notificationCell: UITableViewCell {
    @IBOutlet weak var category: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var customImageView: UIImageView!
    @IBOutlet weak var detailsText: UITextView!
    @IBOutlet weak var callButton: UIButton!
    @IBOutlet weak var cellView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
//    internal var aspectConstraint : NSLayoutConstraint? {
//        didSet {
//            if oldValue != nil {
//                customImageView.removeConstraint(oldValue!)
//            }
//            if aspectConstraint != nil {
//                customImageView.addConstraint(aspectConstraint!)
//            }
//        }
//    }
//    
//    override func prepareForReuse() {
//        super.prepareForReuse()
//        aspectConstraint = nil
//    }
//    
//    func setCustomImage(image : UIImage) {
//
//        let aspect = image.size.width / image.size.height
//
//        let constraint = NSLayoutConstraint(item: customImageView, attribute: NSLayoutConstraint.Attribute.width, relatedBy: NSLayoutConstraint.Relation.equal, toItem: customImageView, attribute: NSLayoutConstraint.Attribute.height, multiplier: aspect, constant: 0.0)
//        constraint.priority = UILayoutPriority(rawValue: 999)
//
//        aspectConstraint = constraint
//
//        customImageView.image = image
//    }

}
