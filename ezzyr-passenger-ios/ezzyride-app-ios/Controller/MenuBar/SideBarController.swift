//
//  SideBarController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 11/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher


class SideBarController: BaseController {
    
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var rideCompleted: UILabel!
    @IBOutlet weak var totalStar: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var TopViewHeight: NSLayoutConstraint!
    @IBOutlet weak var profilePictureImageView: UIImageView!
    
    @IBOutlet weak var closeButt: UIButton!
    
    let placeholder = UIImage(named: "ic_avatar")
    let star = "☆"
    var myTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initSetup()
        profileInfoSetup()
      
    }
    
    func initSetup() {
        self.TopViewHeight.constant =  view.frame.height / 3
        let tapImage = UITapGestureRecognizer(target: self, action: #selector(profileTapped(tapGestureRecognizer:)))
        profilePictureImageView.isUserInteractionEnabled = true
        profilePictureImageView.addGestureRecognizer(tapImage)
        
        let tapName = UITapGestureRecognizer(target: self, action: #selector(userNameTapped(tapGestureRecognizer:)))
        userName.isUserInteractionEnabled = true
        userName.addGestureRecognizer(tapName)
        
        rideCompleted.layer.cornerRadius = rideCompleted.frame.size.height / 2
        rideCompleted.layer.masksToBounds = true
        
        totalStar.layer.cornerRadius = totalStar.frame.size.height / 2
        totalStar.layer.masksToBounds = true
        
        profilePictureImageView.layer.cornerRadius = profilePictureImageView.frame.size.height / 2
        profilePictureImageView.layer.masksToBounds = true
        closeButt.addTarget(self, action: #selector(onSlideMenuButtonPressed(_:)), for: .touchUpInside)
    }

    
    func profileInfoSetup() {
        let userinfo = Helpers.getStringValueForKey(Constants.PROFILE_INFO)
        let userInfoJSON = JSON.init(parseJSON: userinfo)
        if !userInfoJSON.isEmpty {
            if let profile_picture_url = userInfoJSON["profile_pic_url"].string {
                let urlString = profile_picture_url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                let url = URL(string: urlString!)
                profilePictureImageView.kf.indicatorType = .activity
                profilePictureImageView.kf.setImage(with: url, placeholder: placeholder)
            }
            else {
                profilePictureImageView.image = placeholder
            }
            
            var fullName = ""
            if let first_name = userInfoJSON["first_name"].string {
                fullName = first_name
            }
            if let last_name = userInfoJSON["last_name"].string {
                if fullName.count > 0 {
                    fullName = fullName + " " + last_name
                }
            }
            userName.text = fullName.uppercased()
            
            var ridesCompleted = ""
            if let trip_count = userInfoJSON["trip_count"].int {
                ridesCompleted = "   " + String(trip_count) + " Rides Completed   "
                rideCompleted.text = ridesCompleted
            }
            else if let trip_count = userInfoJSON["trip_count"].string {
                ridesCompleted = "   " + trip_count + " Rides Completed   "
                rideCompleted.text = ridesCompleted
            }
            else {
                ridesCompleted = "   0 Rides Completed   "
                rideCompleted.text = ridesCompleted
            }
            
            if let rating_value = userInfoJSON["rating_value"].string {
                let rating = "   " + rating_value + " " + star + "   "
                totalStar.text = rating
            }
        }
    }
    
    func navigationToEditProfileController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editProfileController = mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileController
        self.navigationController?.pushViewController(editProfileController!, animated: true)
    }
    
    
    @objc func profileTapped(tapGestureRecognizer: UITapGestureRecognizer) {
      
        navigationToEditProfileController()
        print("clicked")
    }
    
    @objc func userNameTapped(tapGestureRecognizer: UITapGestureRecognizer) {
        navigationToEditProfileController()
    }

    @IBAction func closeButton(_ sender: Any) {
        (parent as? HomeController)?.showAndHide()
        
    }
}


extension SideBarController : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Constants.menuItem.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? menuCell
        cell?.name.text = Constants.menuItem[indexPath.row]
        cell?.Icon.image = UIImage(named: "\(Constants.menuItemImage[indexPath.row]).png")
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let index = indexPath.row

        if index == 0{
            navigationToEditProfileController()
        }else if index == 1{
            Helpers.removeValue(Constants.PROFILE_INFO)
            Helpers.removeValue(Constants.USER_ID)
            UIApplication.shared.keyWindow?.rootViewController = initViewController()
        }else{
           
        }
    }
    
    func navigateToNotificationController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editProfileController = mainStoryboard.instantiateViewController(withIdentifier: "notification") as? notificationController
        self.navigationController?.present(editProfileController!, animated: true, completion: nil)
    }
    
    func navigationToAboutController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "sideBar", bundle: nil)
        let navigationCon = mainStoryboard.instantiateViewController(withIdentifier: "about") as! AboutViewController
        self.navigationController?.pushViewController(navigationCon, animated: true)

    }
    func initViewController() -> UINavigationController {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let splashController = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [splashController!]
        return navigationController

    }




}





