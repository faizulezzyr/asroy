//
//  PhoneNumberController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 6/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

var numberForForgotPassord = ""
class PhoneNumberController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var NavigationTitle: UILabel!
    @IBOutlet weak var submit: UIButton!
    @IBOutlet weak var numberText: UITextField!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var one: UIButton!
    @IBOutlet weak var two: UIButton!
    @IBOutlet weak var Three: UIButton!
    @IBOutlet weak var four: UIButton!
    @IBOutlet weak var five: UIButton!
    @IBOutlet weak var six: UIButton!
    @IBOutlet weak var seven: UIButton!
    @IBOutlet weak var eight: UIButton!
    @IBOutlet weak var nine: UIButton!
    @IBOutlet weak var zero: UIButton!
    @IBOutlet weak var cross: UIButton!
    @IBOutlet weak var numberView: UIView!
    @IBOutlet weak var verificationCodeView: UIView!
    @IBOutlet weak var ShowSendNumber: UILabel!
    @IBOutlet weak var verifyCode1: UITextField!
    @IBOutlet weak var verifyCode2: UITextField!
    @IBOutlet weak var verifyCode3: UITextField!
    @IBOutlet weak var verifyCode4: UITextField!
    @IBOutlet weak var verifyCode5: UITextField!
    @IBOutlet weak var numberValidationMessage: UILabel!
    @IBOutlet weak var timeCounter: UILabel!
    @IBOutlet weak var ResendButton: UIButton!
    
    
    var viewTrack = 0
    var buttonTappdValue = ""
    var count = 50
    
    var ONE_TIME_PASSWORD = ""
    
    var authenticationService: AuthenticationService?
    var apiCommunicatorHelper: APICommunicator?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initApiCommunicatorHelper()
        verifyCode1.delegate = self
        verifyCode2.delegate = self
        verifyCode3.delegate = self
        verifyCode4.delegate = self
        verifyCode5.delegate = self
        if #available(iOS 12.0, *) {
            verifyCode1.textContentType = .oneTimeCode
        } else {
            // Fallback on earlier versions
        }
        uiChange()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
    }
    //MARK: API Implementation for verified code
    
    func doValidateCode(_ code: String){
        let params = [
            "mobile_no": numberText.text ?? "",
            "user_type": String(UserType.One.rawValue),
            "code": code
            
        ]
        
        authenticationService?.doValidateCode(params)
    }
    
    func getValideCodeResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
  
            print(response)
            if !response.isEmpty {
                if let code = response["code"].string {
                    if Int(code) == Constants.STATUS_CODE_SUCCESS {
                        if let success = response["success"].bool {
                            if success {
                                if let authCode = response["authentication_code"].string {
                                    authcodeString = authCode
                                    print(authcodeString)
                                }
                                navigationToRegisterController()
                            }
                            else {
                                //msg Invalid Code
                                let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.INVALID_AUTH_CODE)
                                self.present(popup, animated: true, completion: nil)
                            }
                        }
                    }
                }
            }
            
        } else {
            //need to handle
        }
    }
    
    //MARK: API Implementation for sms code
    
    func doValidatePhoneNumber(_ mobileNumber: String) {
        
        let params = [
            "mobile_no": mobileNumber,
            "user_type": String(UserType.One.rawValue)
            
        ]
        authenticationService?.doValidatePhoneNumber(params)
    }
    
    func getValidatePhoneNumberResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            print(response)
            if !response.isEmpty {
                if let code = response["code"].string {
                    if Int(code) == Constants.STATUS_CODE_SUCCESS {
                        var signIn = false
                        var signUp = false
                        var signUpData = false
                        
                        if let signin = response["signin"].bool {
                            signIn = signin
                        }
                        
                        if let signup = response["signup"].bool {
                            signUp = signup
                        }
                        
                        if let signup_data = response["signup_data"].bool {
                            signUpData = signup_data
                        }
                        
                        if !signIn && signUp {
                            numberView.isHidden = true
                            verificationCodeView.isHidden = false
                            viewTrack = viewTrack + 1
                        }
                        else if signIn && !signUp {
                     
                            navigateToLoginController()
                        }
                        else if !signIn && !signUp && signUpData {
                            navigationToRegisterController()
                            if let authCode = response["authentication_code"].string {
                                authcodeString = authCode
                                print(authcodeString)
                            }
                        }
                    }
                    else {
                        //need to handle
                    }
                }
            }
        }
        else {
            //need to handle
        }
    }
    
    func uiChange(){
        
        Commons.bottomCornerRedious(button: submit, cornerRedious: 20)
        let change = Commons()
        change.placeHolderColorChange(TextFild: numberText, PlaceHolder: "01XXXXXXXXX")
        verificationCodeView.isHidden = true
        ResendButton.isUserInteractionEnabled = false
    }
    
    func navigateToLoginController() {
        
        let phoneNumber = numberText.text ?? ""
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginController = mainStoryboard.instantiateViewController(withIdentifier: "LoginVC") as? LoginController
        loginController?.mobileNumber = phoneNumber
        self.navigationController?.pushViewController(loginController!, animated: true)
    }
    
    func navigationToRegisterController() {
        let mobileNumber = numberText.text ?? ""
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let registerController = mainStoryboard.instantiateViewController(withIdentifier: "RegisterVC") as? RegisterController
        registerController?.mobileNumber = mobileNumber
        //        registerController?.authenticationCode = ONE_TIME_PASSWORD
        self.navigationController?.pushViewController(registerController!, animated: true)
    }
    
    ///For taking verification code manually
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if ((textField.text?.count)! < 1  && string.count > 0){
            if(textField == verifyCode1){
                verifyCode1.text = buttonTappdValue
                verifyCode2.becomeFirstResponder()
            }
            if(textField == verifyCode2){
                verifyCode3.becomeFirstResponder()
            }
            if(textField == verifyCode3){
                verifyCode4.becomeFirstResponder()
            }
            if (textField == verifyCode4){
                verifyCode5.becomeFirstResponder()
            }
            textField.text = string
            return false
            
        }else if ((textField.text?.count)! >= 1  && string.count == 0){
            // on deleting value from Textfield
            if(textField == verifyCode2){
                verifyCode2.becomeFirstResponder()
            }
            if(textField == verifyCode3){
                verifyCode2.becomeFirstResponder()
            }
            if(textField == verifyCode4) {
                verifyCode3.becomeFirstResponder()
            }
            if (textField == verifyCode5){
                verifyCode4.becomeFirstResponder()
            }
            textField.text = ""
            return false
        }else if ((textField.text?.count)! >= 1  ){
            textField.text = string
            return false
        }
        return true
    }
    
    
    @IBAction func numberButton(_ sender: Any) {
        if viewTrack == 0 {
            
            if let buttonTitle = (sender as AnyObject).title(for: .normal) {
                
                let number = self.numberText.text ?? ""
                if number.count < 11 {
                    self.numberText.text?.append(buttonTitle)
                }
                changeBackOnTap(button: one)
                changeBackOnTap(button: two)
                changeBackOnTap(button: Three)
                changeBackOnTap(button: four)
                changeBackOnTap(button: five)
                changeBackOnTap(button: six)
                changeBackOnTap(button: seven)
                changeBackOnTap(button: eight)
                changeBackOnTap(button: nine)
                changeBackOnTap(button: cross)
                changeBackOnTap(button: zero)
            }
        }else{
            if let buttonTitle = (sender as AnyObject).title(for: .normal) {
                if verifyCode1.text == ""{
                    self.verifyCode1.text = buttonTitle
                }
                else if verifyCode2.text == ""{
                    self.verifyCode2.text = buttonTitle
                }
                else if verifyCode3.text == ""{
                    self.verifyCode3.text = buttonTitle
                }
                else if verifyCode4.text == ""{
                    self.verifyCode4.text = buttonTitle
                }
                else if verifyCode5.text == ""{
                    self.verifyCode5.text = buttonTitle
                }
            }
        }
        
    }
    
    
    @IBAction func deleteNumber(_ sender: Any) {
        var totalNumber = self.numberText.text ?? ""
        if totalNumber.count > 0 {
            totalNumber = String(totalNumber.dropLast())
            self.numberText.text = totalNumber
        }
    }
    
    @IBAction func SubmitButton(_ sender: Any) {
        if viewTrack == 0 {
            if (numberText.text?.count)! < 11{
                self.numberValidationMessage.text = "Please enter a valid phone number"
            }
            else{
                
                self.ShowSendNumber.text = "Enter the code sent to \(numberText.text ?? "")"
                self.NavigationTitle.text = "Verify phone number"
                
                _ = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(update), userInfo: nil, repeats: true)
                
                let phoneNumber = numberText.text ?? ""
                if phoneNumber.count > 0 {
                    doValidatePhoneNumber(phoneNumber)
                    numberForForgotPassord = phoneNumber
                }
            }
        }
        else{
            
            let code1 = verifyCode1.text ?? ""
            let code2 = verifyCode2.text ?? ""
            let code3 = verifyCode3.text ?? ""
            let code4 = verifyCode4.text ?? ""
            let code5 = verifyCode5.text ?? ""
            
            if code1.count > 0 && code2.count > 0 && code3.count > 0 && code4.count > 0 && code5.count > 0 {
                let bindAuthCode = code1 + code2 + code3 + code4 + code5
                ONE_TIME_PASSWORD = bindAuthCode

                doValidateCode(bindAuthCode)
            }
            else {
                // show message for not entering all the input fields value.
                let popup = CustomAlertPopup.customOkayPopup("", msgBody: Constants.MISSING_MANDATORY_FIELD)
                self.present(popup, animated: true, completion: nil)
            }
        }
    }
    
    @objc func update() {
        if(count > 0) {
            count -= 1
            timeCounter.text = "Resend code in \(count)"
            
        }
        if count == 0 {
            ResendButton.isUserInteractionEnabled = true
            timeCounter.isHidden = true
        }
    }
    
    @IBAction func resendtapd(_ sender: Any) {
        
        let phoneNumber = numberText.text ?? ""
        if phoneNumber.count > 0 {
            doValidatePhoneNumber(phoneNumber)
        }
    }
    
    
    func changeBackOnTap(button: UIButton){
        button.setBackgroundColor(color: UIColor(red:2, green:153, blue:52, alpha:1.0), forState: .highlighted)
    }
    
    @IBAction func Back(_ sender: Any) {
        
        if viewTrack == 0 {
            self.iniitializeGetStart()
        }else if viewTrack == 1{
            
            self.NavigationTitle.text = "Get Started"
            self.numberValidationMessage.text = "we'll send a sms to verify your number"
            viewTrack = 0
            numberView.isHidden = false
            verificationCodeView.isHidden = true
            count = 50
        }
    }
    
    private func iniitializeGetStart() {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [viewController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
}

extension PhoneNumberController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getValidatePhoneNumberResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SECOND {
            getValideCodeResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.THIRD {
            
        }
    }
}













