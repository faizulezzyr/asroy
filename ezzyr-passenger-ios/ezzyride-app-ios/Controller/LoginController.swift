//
//  LoginController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 10/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
var loginTace = 0
 var isFcmChange = false
class LoginController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    
    var mobileNumber = ""
    
    var authenticationService: AuthenticationService?
    var apiCommunicatorHelper: APICommunicator?
    var stageService: StageService?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        uiChange()
        initApiCommunicatorHelper()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        passwordText.resignFirstResponder()
        return true
    }
    func uiChange(){
        self.passwordText.delegate = self
        let changeColor = Commons()
        changeColor.placeHolderColorChange(TextFild: passwordText, PlaceHolder: "PASSWORD")
        Commons.bottomCornerRedious(button: submitButton, cornerRedious: 20)
    }
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        authenticationService = AuthenticationService(self.view, communicator: apiCommunicatorHelper!)
        stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    func navigateToDashboard() {
            let main: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let home = main.instantiateViewController(withIdentifier: "HomeV") as? HomeController
            let navigationController = UINavigationController(rootViewController: home!)
            self.navigationController?.present(navigationController, animated: true, completion: nil)
    }
    
    //MARK: API Implementation
    
    func doLogin(_ password: String) {
        
        let params = [
            "mobile_no": mobileNumber,
            "password": password,
            "user_type": String(UserType.One.rawValue)
        ]
        authenticationService?.doLogin(params)
    }
    
    func getLoginResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            var success = false
            if !response.isEmpty {
                if let loginStatus = response["success"].bool {
                    success = loginStatus
                    if success == true {
                        
                        if let userInfo = response.rawString() {
                            Helpers.setStringValueWithKey(userInfo, key: Constants.USER_INFO)
                        }
                        
                        if let token = response["token"].string {
                            Helpers.setStringValueWithKey(token, key: Constants.ACCESS_TOKEN)
                        }
                        
                        if let member_type = response["member_type"].int {
                            Helpers.setIntValueWithKey(member_type, key: Constants.MEMBER_TYPE)
                        }
                        else if let member_type = response["member_type"].string {
                            Helpers.setIntValueWithKey(Int(member_type)!, key: Constants.MEMBER_TYPE)
                        }
                        
                        if let member_id = response["member_id"].int {
                            Helpers.setIntValueWithKey(member_id, key: Constants.MEMBER_ID)
                            updateToken()
                        }
                        else if let member_id = response["member_id"].string {
                            Helpers.setIntValueWithKey(Int(member_id)!, key: Constants.MEMBER_ID)
                            updateToken()
                        }
                        
                        if let user_id = response["user_id"].int {
                            Helpers.setIntValueWithKey(user_id, key: Constants.USER_ID)
                        }
                        else if let user_id = response["user_id"].string {
                            Helpers.setIntValueWithKey(Int(user_id)!, key: Constants.USER_ID)
                        }
                        
                        if let stageData = response["stage_data"].dictionary {
                            if let driverStage = stageData["driver_stage"]?.int {
                                if driverStage != 0 {
                                  requestForStage()
                                    loginTace = 1
                                }else {
                                   
                                         navigateToDashboard()
                                }
                            }
                        }
                        
                    }else{
                        if let reson = response["msg"].string {
                            let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                            self.present(popup, animated: true, completion: nil)
                        }
                   }
               }
            }
        }
        else {
            //need to handle
        }
    }
    
    func updateToken(){
        
        let id = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        let token = Helpers.getStringValueForKey(Constants.FCM_TOKEN)
        print("this is token\(token)")
        let params = [
        "device_id" : token,
        "member_id" : String(id)
        ]
        self.authenticationService!.doUpdateToken(params)
    }
    func requestForStage(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "passenger_id" : userId
        ]
        self.stageService?.doStageRequest(params)
        
    }
    
    func navigateToRestController() {

        let resetView: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let reset = resetView.instantiateViewController(withIdentifier: "resetView") as? ResetPasswordController
        self.navigationController?.pushViewController(reset!, animated: true)
    }
    // MARK: API Implemantion for send OTP
    func doValidatePhoneNumber(_ mobileNumber: String) {
        
        let params = [
            "mobile_no": mobileNumber,
            "user_type": String(UserType.One.rawValue)
            
        ]
        authenticationService?.doForgotPasswordOtpRequest(params)
    }
    
    func getValidatePhoneNumberResponse(_ data: [String: Any], statusCode: Int) {
        
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            var success = false
            if !response.isEmpty {
                if let code = response["code"].string {
                    if Int(code) == Constants.STATUS_CODE_SUCCESS {
                        if let loginStatus = response["success"].bool {
                            success = loginStatus
                            if success == true {
                                navigateToRestController()
                            }else {
                                if let reson = response["message"].string {
                                    let popup = CustomAlertPopup.customOkayPopup("", msgBody: reson)
                                    self.present(popup, animated: true, completion: nil)
                                }

                            }
                        }
                    }
                    else {
                        //need to handle
                    }
                }
            }
        }
        else {
            //need to handle
        }
    }
    
    func getUserStageDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            if !response.isEmpty {
                if let stageData = response.rawString() {
                    Helpers.removeValue(Constants.STAGE_DATA)
                    Helpers.setStringValueWithKey(stageData, key: Constants.STAGE_DATA)
                         navigateToDashboard()
                }
            }
        }
    }
    func getTokenUpdteResponds(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            print(response)
        }
    }
    @IBAction func backButtonPressed(_ sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submit(_ sender: Any) {
        let password = passwordText.text ?? ""
        if password.count > 0 {
            doLogin(password)
        }
     }
    
    @IBAction func forgotPassword(_ sender: Any) {
        doValidatePhoneNumber(numberForForgotPassord)
    }
}

extension LoginController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getLoginResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SECOND {
            //getUserZoneResponse(data, statusCode: statusCode)
        } else if methodTag == MethodTags.THIRD {
            getValidatePhoneNumberResponse(data, statusCode: statusCode)
        } else if methodTag == MethodTags.ELEVENTH {
            getUserStageDataResponse(data, statusCode: statusCode)
        } else if methodTag == MethodTags.FIFTH {
            getTokenUpdteResponds(data, statusCode: statusCode)
        }
    }
}




