//
//  FareAndSubmissionViewController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 19/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher


class FareAndSubmissionViewController: UIViewController {

    @IBOutlet weak var ratingView: UIView!
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var submitButton: UIButton!

    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var passengerTotalFare: UILabel!
   
//    var tripId =
    
    var userStage: JSON = [:]
    var apiCommunicatorHelper: APICommunicator?
    var bookingService: BookingService?
    var stageService: StageService?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Commons.makeCardView(view: ratingView)
        Commons.bottomCornerRedious(button: submitButton, cornerRedious: 5)
   
        initApiCommunicatorHelper()
        driverImage.layer.cornerRadius = 29
        driverImage.clipsToBounds = true
        let stageInfo = Helpers.getStringValueForKey(Constants.STAGE_DATA)
        userStage = JSON.init(parseJSON: stageInfo)
        
        
    }
    func requestForStage(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "passenger_id" : userId
        ]
        self.stageService?.doStageRequest(params)
        
    }

 
    
    func initApiCommunicatorHelper() {
        apiCommunicatorHelper = APICommunicator(view: self.view)
        apiCommunicatorHelper?.delegate = self
        bookingService = BookingService(self.view, communicator: apiCommunicatorHelper!)
        self.stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        showData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    func showData(){
        
        let stageInfo = Helpers.getStringValueForKey(Constants.STAGE_DATA)
        userStage = JSON.init(parseJSON: stageInfo)
        print(userStage)
        
        if let Data = userStage["data"].dictionary{
            if let stageData = Data["stage_data"]?.dictionary{
                if let driverStage = stageData ["driver_stage"]?.int{
                    if driverStage == 1 || driverStage == 2 || driverStage == 3 || driverStage == 4 || driverStage == 0 {
                        requestForStage()
                    }
                    
                }
                if let pessengerObj = stageData["passenger_object"]?.dictionary{
                    if let stage2_2 = pessengerObj["stage_2_2"]?.dictionary{
                        if let dataDriverDetails = stage2_2["data_driver_details"]?.dictionary{
                            if let code = dataDriverDetails["code"]?.int{
                                if code == Constants.SUCCESS_STATUS_CODE {
                         
                                    if let Profileurl = dataDriverDetails["profile_pic_url_alt"]?.string{
                                        let urlString = Profileurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                        let url = URL(string: urlString!)
                                        self.driverImage.kf.setImage(with: url)
                                    }
                                    if let activeVahicale = dataDriverDetails["active_vehicle"]?.dictionary{
                                        if let model = activeVahicale["model"]?.string{
                                            
                                        }
                                  
                                        if let vehicalType = activeVahicale["category_type"]?.string{
                                           
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
                }

            }
            
        }
        
    }

    
    @IBAction func submit(_ sender: Any) {
           self.submitRating()
    }

    func submitRating(){
        let pID = Helpers.getIntValueForKey(Constants.USER_ID)
        let tripRequestId = Helpers.getIntValueForKey(Constants.TRIP_REQUEST_ID)

        print("this is trip id\(tripRequestId)")
        let driverId = Helpers.getStringValueForKey(Constants.DRIVER_ID)
       
        let params = [
            "passenger_id" : String(pID),
            "driver_id" : String(driverId),
            "trip_request_id": String(tripRequestId),
            "review_value" : String(cosmosView.rating),
            "review_by":"1",
            "comments":"No comments"
        ]
        bookingService?.doReview(_params: params)
        print(params)
    }
    
    func getRatingReponds (_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
               print(response)
            if let success = response["success"].bool{
                if success == true{
        
                    destroyData()
                    let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                    self.present(popup, animated: true, completion: nil)
                    navaigationToHome()
                    
                }else{
                    let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                    self.present(popup, animated: true, completion: nil)
                }
            }
      
            
        }
        
    }
    // User Stage Responds
    func getUserStageDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            
            if !response.isEmpty {
                if let stageData = response.rawString() {
                    Helpers.removeValue(Constants.STAGE_DATA)
                    Helpers.setStringValueWithKey(stageData, key: Constants.STAGE_DATA)
                    showData()
                }
            }
        }
    }
    
    func destroyData(){
        Helpers.removeValue(Constants.TRIP_ID)
        Helpers.removeValue(Constants.DRIVER_ID)
        Helpers.removeValue(Constants.STAGE_DATA)
        Helpers.removeValue(Constants.TRIP_REQUEST_ID)
    }
    func navaigationToHome() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let homeController = mainStoryboard.instantiateViewController(withIdentifier: "HomeV") as? HomeController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [homeController!]
        UIApplication.shared.keyWindow?.rootViewController = navigationController
    }
    

}

extension  FareAndSubmissionViewController: APICommunicatorDelegate{
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST{
          getRatingReponds(data, statusCode: statusCode)
        } else if methodTag == MethodTags.ELEVENTH {
            getUserStageDataResponse(data, statusCode: statusCode)
        }

        
    }
    
    
}

