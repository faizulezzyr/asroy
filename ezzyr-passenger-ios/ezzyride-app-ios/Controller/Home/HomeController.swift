//
//  HomeController.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 11/11/18.
//  Copyright � 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import Kingfisher
import Lottie
import PubNub
import GoogleUtilities
import LSDialogViewController
import MessageUI
public var cellIndex = 0
public var traceCurrentPage = 0
public var FareListArry: [JSON] = []
public var driverPhone = ""
var isOpen = false
var vahicleGroupId = 0
var BackStage = 0
var markerIconName = ""
var totalReturnDate = 0
var isProfilePicAvailAble = false
var myCurrentLocation: CLLocationCoordinate2D?
class HomeController: BaseController,GMSMapViewDelegate,MFMessageComposeViewControllerDelegate {
    //Fare details Outlet

    //// other outlet
    @IBOutlet weak var anView: UIView!
    @IBOutlet weak var driverProfile: UIView!
    
    @IBOutlet var myView: GMSMapView!
    @IBOutlet weak var requestForTripButton: UIButton!
    @IBOutlet weak var homeBack: UIImageView!
    @IBOutlet weak var homeBackView: UIView!
    
    @IBOutlet weak var internetErrorView: UIView!
    @IBOutlet weak var internetErrorLabel: UILabel!
    @IBOutlet weak var mbutton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var victimeImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var driverImage: UIImageView!
    @IBOutlet weak var driverName: UILabel!
    @IBOutlet weak var driverDesignation: UILabel!
    @IBOutlet weak var victimeEContact: UILabel!
    @IBOutlet weak var victimeAddress: UILabel!
    @IBOutlet weak var internetMsg: UIView!
    
    
    var pickupLocation = PickupLocationModel()
    var dropLocation = DropupLocationModel()
    var calculatedDistance: Double = 0.0
    var calculateMinute: Double = 0.0

    var memberId = 0
    var userService: UserService?
    var apiCommunicatorHelper: APICommunicator?
    var settingsService: SettingsService?
    var directionService: DirectionService?
    var placeSearchService: PlaceSearchService?
    var bookingService: BookingService?
    var stageService: StageService?
    var fakeCar : FakeCarService?
    var currentLocation: CLLocationCoordinate2D? = nil
    var locationManager: CLLocationManager!
    var currentLocationMarker: GMSMarker?
    var firstTimeLocation = true
    var topBack = 0
    var origin = ""
    var destination = ""

    let geocoder = GMSGeocoder()
    var markLet: Double?
    var markLng: Double?
    var pickUpCoordinate: CLLocationCoordinate2D?
    var destinationCoordinate: CLLocationCoordinate2D?
    var userStage: JSON = [:]
  
   
    var pickupAddrsss = "" 
    var myTimer = Timer()
    var StaticDriverStage = 0
    var cancelRequestLabel = 1
    var notificationImage = "bell"
    var client : PubNub?
    var config : PNConfiguration?
    var driverMarker = GMSMarker()
    var newLet : String?
    var newLng : String?
    var oldLet : String?
    var oldLng : String?
    let logo = UIImage(named: "ic_launcher")!
    var isStageData: Bool = false
    var autoCompleateLocationCordinate: CLLocationCoordinate2D?
    var driverSearchLottiAnimation: AnimationView?
    let piker = UIDatePicker()
    var locationArray = [[Double]]()
    var objectDivisionJSON: JSON = [:]
    let path = GMSMutablePath()
    let subZonePath = GMSMutablePath()
    var pickUpSubzone = ""
    let alart = SweetAlert()


    // variable for picup and drop

    
    var homeApiRequest = 0
    var isSwithcOn = false
    var isSetuPPickupAndDestination = false
    let dialogViewController = ErrorView(nibName: "serverErrorMsg", bundle: nil)
    let NotFoundDialog = driverNotFoundView(nibName: "DriverNotFound", bundle: nil)


    var isStartRequestProcess = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initGester()
        initApiCommunicatorHelper()
        doRegisterForReceivingPushNotification()
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        mbutton.addTarget(self, action: #selector(showAndHide), for: .touchUpInside)
        notificationButton.addTarget(self, action:#selector(navigationTonotificationController), for: .touchUpInside)
        initGoogleMap()
        initializeOtherView()
        getProfile()
        profileInfoSetup()

     

    }
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
         controller.dismiss(animated: true)
    }

    func displayMessageInterface() {
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let userPhone = Helpers.getStringValueForKey(Constants.USER_PHONE)
        let composeVC = MFMessageComposeViewController()
        composeVC.messageComposeDelegate = self
        
        // Configure the fields of the interface.
        composeVC.recipients = ["26969"]
        composeVC.body = """
        RAOS "mobile":"\(userPhone)","victim":"\(userId)","latitude":"\(markLet!)","longitude":"\(markLng!)","thana":"\(pickUpSubzone)"
        """
        
        // Present the view controller modally.
        if MFMessageComposeViewController.canSendText() {
            self.present(composeVC, animated: true, completion: nil)
        } else {
            print("Can't send messages.")
        }
    }
    @objc func showAndHide(){
        
        if(!isOpen)
            
        {
            isOpen = true
            
            let menuVC : SideBarController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! SideBarController
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()
            
            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width+100, height: UIScreen.main.bounds.size.height);
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            }, completion:nil)
            
        }else if(isOpen)
        {
            isOpen = false
            let viewMenuBack : UIView = view.subviews.last!
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
                
            })
        }
    }

     func showDialog(_ animationPattern: LSAnimationPattern) {
        let dialogViewController = SuccessView(nibName: "BookingSuccess", bundle: nil)
        dialogViewController.delegate = self
//        dialogViewController.text.text = "Your schedule trip is placed successfully. You will find the details of your trip on upcoming trip list."
        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
    }
    func dismissDialog() {
        dismissDialogViewController(LSAnimationPattern.fadeInOut)
    }
    func pubInit(Chanal: String) {
        config = PNConfiguration(publishKey: Constants.PUBNUB_PUBLISH_KEY, subscribeKey: Constants.PUBNUB_SUB_KEY)
        client = PubNub.clientWithConfiguration(config!)
       // client!.subscribeToChannels([Chanal], withPresence: false)
        let publishLetLng = [
            "lat" : "\(markLet ?? 0.0)",
            "lng" : "\(markLng ?? 0.0)",
            "alt" : "0.0"
              ] as [String : Any]
        
        
        client!.publish(publishLetLng, toChannel: Chanal, compressed: false, withCompletion: nil)
        client!.addListener(self)
    }
    
  
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
    }
  
    func driverDetails(){
        let RequestID = Helpers.getIntValueForKey(Constants.TRIP_ID)
        stageService?.doDriverDetailsRequest(id: RequestID)
    }
    func addDriveInfo() {
         driverDetails()
        driverProfile.isHidden = false
    }
    func removerDriverInfo(){
        driverProfile.isHidden = true
        Helpers.removeValue(Constants.DRIVER_ID)
    }

    func doRegisterForReceivingPushNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.doReceivePayload(_:)), name: .pushPayload, object: nil)
    }
    
    @objc func doReceivePayload(_ notification: NSNotification) {
        
        if let info = notification.userInfo as? [String: Any] {

           
            if let msg = info["message"] as? String {
                let message = JSON.init(parseJSON: msg)
                if let action = message["action"].string {
                    if action == "accept_trip_request" {
                       self.stopAnimationLotti()
                         showTop()
                        //pubInit(Chanal: driverChanelName)
                        if let submsg = message["msg"].dictionary {
                        if let driverLet = submsg["latitude"]?.string {
                            if let driverLng = submsg["longitude"]?.string {
                                 DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                    self.Animation(Lat: Double(driverLet)!, lng: Double(driverLng)!, oldLat: Double("6.6")!, OldLng: Double("5.9")!)
                                     self.addDriveInfo()
                                }
                             }
                           }
                        }
                       // driverMarker.position = cordinate
                        
                    }else if action == "ready_to_pickup" {
                        self.removerDriverInfo()
                         navigateToFareAndRating()
                    }else if action == "trip_start" {
                        
                        
                    }else if action == "end_trip"{
                     self.removerDriverInfo()
                      navigateToFareAndRating()
                    }else if action == "cancel_trip_request" {
                        hideTop()
                        removerDriverInfo()
                         doTripRequest(RequestType: "0")
                    }else if action == "new_content_notification"{
                        var noti =  Helpers.getIntValueForKey(Constants.TOTAL_NOTIFICATION)
                        if noti == -1 {
                            noti = 0
                        }
                        let NotiPluse = Int(noti) + 1
                        Helpers.removeValue(Constants.TOTAL_NOTIFICATION)
                        Helpers.setIntValueWithKey(NotiPluse, key: Constants.TOTAL_NOTIFICATION)
                    }else if action == "driver_appply" {
                        
                    }
                }
            }
        }
    }
    

    
    func initGester(){
        let leftSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        let rightSwipe = UISwipeGestureRecognizer(target: self, action: #selector(moveToNextItem(_:)))
        
        leftSwipe.direction = .left
        rightSwipe.direction = .right
        
        self.view.addGestureRecognizer(leftSwipe)
        self.view.addGestureRecognizer(rightSwipe)
    }
    @objc func moveToNextItem(_ sender:UISwipeGestureRecognizer) {
        switch sender.direction{
        case .left:
         
            if BackStage == 0 {
                if isStageData == false{
                   showAndHide()
                }
            }
            
        case .right:
            if BackStage == 0 {
                if isStageData == false{
                    showAndHide()
                }
            }
           
        default: break //default
        }
        
    }
    
    @objc func navigationTonotificationController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editProfileController = mainStoryboard.instantiateViewController(withIdentifier: "notification") as? notificationController
        self.navigationController?.present(editProfileController!, animated: true, completion: nil)
        Helpers.removeValue(Constants.TOTAL_NOTIFICATION)
        
    }
    func initializeOtherView(){
        Commons.makeCardView(view: homeBackView)
        driverImage.layer.cornerRadius = 30
        driverImage.clipsToBounds = true
        victimeImage.layer.cornerRadius = 60
        victimeImage.clipsToBounds = true
        self.anView.isHidden = true
        let now = Date()
        piker.minimumDate = now
        requestForTripButton.layer.cornerRadius = 30
        self.internetErrorView.isHidden = true
        dialogViewController.homeDelegate = self
        NotFoundDialog.homeViewDelegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(self.tryAgainNoti), name: NSNotification.Name(rawValue: "serverError"), object: nil)
        driverProfile.isHidden = true
        internetMsg.isHidden = true

    }
    var NumberOfAlart = 0
    @objc func tryAgainNoti(notif: NSNotification) {
        if  NumberOfAlart == 0{
            showInernetDialog(.slideBottomTop)
        }
    }
  
    func showInernetDialog(_ animationPattern: LSAnimationPattern) {
        presentDialogViewController(dialogViewController, animationPattern: animationPattern)
        globalViewControllerString = "HomeController"
        print(NumberOfAlart)
         NumberOfAlart += 1
    
    }
   
    
    func showNotFoundDailog(_ animationPattern: LSAnimationPattern) {
        presentDialogViewController(NotFoundDialog, animationPattern: animationPattern)
    }
    
    func dismissInternetDialog() {
       NumberOfAlart = 0
        switch homeApiRequest {
        case 1:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
          
            
        case 2:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
            doTripRequest(RequestType: "0")

            
        case 3:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
        
        case 4:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
          
            
        case 5:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
       
           
        case 6:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
          
           
        case 7:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
            getProfile()
            
        default:
            dismissDialogViewController(LSAnimationPattern.fadeInOut)
        }
    
    }

    deinit {
        NotificationCenter.default.removeObserver(self, name: .pushPayload , object: nil)
        NumberOfAlart = 0
    }
    func navigateToFareAndRating() {
        let main: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let ratingView = main.instantiateViewController(withIdentifier: "FareAndRating") as? FareAndSubmissionViewController
        let navigationController = UINavigationController(rootViewController: ratingView!)
        self.navigationController?.present(navigationController, animated: true, completion: nil)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
      
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.setNavigationBarHidden(false, animated: animated)

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        if CLLocationManager.locationServicesEnabled() {
            startMonitoringLocation()
            addCurrentLocationMarker(name: "self_location")
        }
    }
    
    func initApiCommunicatorHelper() {
        self.apiCommunicatorHelper = APICommunicator(view: self.view)
        self.apiCommunicatorHelper?.delegate = self
        self.userService = UserService(self.view, communicator: apiCommunicatorHelper!)
        self.settingsService = SettingsService(self.view, communicator: apiCommunicatorHelper!)
        self.directionService = DirectionService(self.view, communicator: apiCommunicatorHelper!)
        self.placeSearchService = PlaceSearchService(self.view, communicator: apiCommunicatorHelper!)
        self.bookingService = BookingService(self.view, communicator: apiCommunicatorHelper!)
        self.stageService = StageService(self.view, communicator: apiCommunicatorHelper!)
        self.fakeCar = FakeCarService(self.view, communicator: apiCommunicatorHelper!)
    }
    
    func initGoogleMap(){
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.myView.isMyLocationEnabled = false
        self.myView.delegate = self
        var region:GMSVisibleRegion = GMSVisibleRegion()
        region.nearLeft = CLLocationCoordinate2DMake(26.633914, 92.6801153)
        region.farRight = CLLocationCoordinate2DMake(20.3794,88.00861410000002)
        let bounds = GMSCoordinateBounds(coordinate: region.nearLeft,coordinate: region.farRight)
        let camera = myView.camera(for: bounds, insets:UIEdgeInsets.zero)
         myView.camera = camera!;
       
        /// var mycamera = GMSCameraPosition.camera(withTarget: bounds, zoom: 6)
        
    }
    
    /// Initialize imageview and constraint


    //MARK: API Implementation
    
    func getProfile() {
        homeApiRequest = 7
        memberId = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        userService?.getProfile(memberId)
    }
    
    @objc func requestForStage(){
        let userId = Helpers.getIntValueForKey(Constants.USER_ID)
        let params = [
            "passenger_id" : userId
        ]
        self.stageService?.doStageRequest(params)
        
    }
    
    func initViewController() -> UINavigationController {
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let splashController = mainStoryboard.instantiateViewController(withIdentifier: "SplashScreenVC") as? SplashScreenController
        let navigationController = UINavigationController()
        navigationController.viewControllers = [splashController!]
        return navigationController
    }

    func alertAction(){
        let dialogMessage = UIAlertController(title: "Attention", message: "Are you sure? You want to make a help request", preferredStyle: .alert)
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            self.doTripRequest(RequestType: "0")
        })
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            print("Cancel button tapped")
        }
        //Add OK and Cancel button to an Alert object
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        // Present alert message to user
        self.present(dialogMessage, animated: true, completion: nil)
    }
    
    @IBAction func requestForTrip(_ sender: Any) {
        if Reachability.isConnectedToNetwork() == true{
            alertAction()
            
        }else{
            displayMessageInterface()
        }
        
    }
    
    @IBAction func driverContact(_ sender: Any) {
        driverPhone.doMakeAPhoneCall()
    }
    
    
    func hideTop(){
        mbutton.isHidden = true
        notificationButton.isHidden = true
    }
    func showTop(){
        mbutton.isHidden = false
        notificationButton.isHidden = false
        homeBack.isHidden = true
        homeBackView.isHidden = true
    }

    let logoImage: UIImageView = {
        let myImage = UIImageView()
        myImage.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        myImage.image = UIImage(named: "ic_launcher")
        myImage.clipsToBounds = true
        myImage.translatesAutoresizingMaskIntoConstraints = false
        
        return myImage
    }()

    func animationLotti(){
        anView.isHidden = false
        driverSearchLottiAnimation = AnimationView(name: "driver_search")
        driverSearchLottiAnimation!.frame = CGRect(x: 0, y: 0, width: self.anView.frame.width, height: self.anView.frame.height)
        driverSearchLottiAnimation!.contentMode = .scaleAspectFit
        driverSearchLottiAnimation!.loopMode = .loop

        self.anView.addSubview(driverSearchLottiAnimation!)
        self.anView.addSubview(logoImage)
         setLogoConstraint()
        driverSearchLottiAnimation!.play()

     
    }
    
    func stopAnimationLotti() {
        anView.isHidden = true
        if driverSearchLottiAnimation != nil {
            driverSearchLottiAnimation?.stop()
            driverSearchLottiAnimation?.removeFromSuperview()
        }
    }

    func ManageHomePageViewAgain(){
        initializeOtherView()
        self.firstTimeLocation = true

        isSetuPPickupAndDestination = false
        self.pickUpCoordinate = nil
        self.destinationCoordinate = nil
        self.zoomToCoordinates(currentLocation!, zoom: 16)
        self.origin = ""
        self.destination = ""
        self.anView.isHidden = true

        self.myView.clear()
        self.myView.reloadInputViews()
        super.viewDidLoad()
        self.locationManager.startUpdatingLocation()
        startMonitoringLocation()
        addCurrentLocationMarker(name: "self_location")

    }


 
    func doTripRequest(RequestType: String) {
        let pID = Helpers.getIntValueForKey(Constants.USER_ID)
        
        let params = [
            "passenger_id": String(pID),
            "latitude":   "\(markLet ?? 0.0)",
            "longitude":  "\(markLng ?? 0.0)", 
            "altitude": "0",
            "location_name": pickupAddrsss ,
            "pickup_zone": "dhaka",
            "pickup_sub_zone": pickUpSubzone,
            "dest_latitude":  "23.750563",
            "dest_longitude": "90.393145",
            "dest_altitude": "0",
            "dest_address": "destinationFormattedAddress",
            "drop_zone":" dropLocation.dropZone",
            "estimated_fare": String(totalFareForDriverDetails), // will take it from response
            "vehicle_type":  "4",  // will take it from input String(vehicleTypeValue)
            "request_type": RequestType,
            "company_id": "0",
            "distance": calculatedDistance,
            "waiting_min": calculateMinute
            ] as [String : Any]
        bookingService?.doBookingRequest(params)
        print(params)
    }
    
    func doCancelRequest(){
        homeApiRequest = 4
        let pID = Helpers.getIntValueForKey(Constants.USER_ID)
        let RequestID = Helpers.getIntValueForKey(Constants.TRIP_REQUEST_ID)
        let params = [
            "request_level": String(cancelRequestLabel),
            "passenger_id": String(pID),
            "request_id": String(RequestID),
            "reason":"Personal reason",
            "pickup_duration":"300000"
        ]
        bookingService?.doCancelRequest(params)
    }


    func loadUserZone(_ lat: String, lon: String) {
        placeSearchService?.getUserZone(lat, lon: lon)
    }

 }




extension HomeController: APICommunicatorDelegate {
    
    func taskCompletationHandler(_ methodTag: Int, data: Dictionary<String, Any>, statusCode: Int) {
        if methodTag == MethodTags.FIRST {
            getProfileResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.ELEVENTH {
            getUserStageDataResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SECOND {
            //getUserZoneResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.THIRD {
            getDirectionResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.FOURTH {
            getDriverResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.SIXTH {
           
        }
        else if methodTag == MethodTags.SEVENTH {
            getTripRequestResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.EIGHTH {
            
        }
        else if methodTag == MethodTags.NINETH {
            
        }
        else if methodTag == MethodTags.TENTH {
              getTripCancelResponse(data, statusCode: statusCode)
        }
        else if methodTag == MethodTags.THIRTEEN {
            
        }
        else if methodTag == MethodTags.FOURTEEN {
            
        }
    }
}





