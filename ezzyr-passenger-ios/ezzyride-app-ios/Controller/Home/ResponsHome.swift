//
//  extentionHome.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 24/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import SwiftyJSON
import GoogleMaps
import CoreLocation
import GooglePlaces
import Alamofire
import Kingfisher
import Lottie



extension HomeController {
   
    func getAddressForLiveLocation(Location: CLLocationCoordinate2D?){
        geocoder.reverseGeocodeCoordinate(Location!)  { response, error in
            if error != nil {
                print("reverse geodcode fail: \(error!.localizedDescription)")
            } else {
                if let places = response?.results() {
                    if let place = places.first {
                        if let lines = place.lines {
                            self.pickupAddrsss = lines[0]
                
                        }
                    } else {
                        print("GEOCODE: nil first in places")
                    }
                } else {
                    print("GEOCODE: nil in places")
                }
            }
        }
    }
    
    
    
    
    
    //MARK:- Api implementation for Direction
    
    
    
    func getDirectionResponse (_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
        
            if !response.isEmpty {
                let routes = response["routes"].arrayValue
                
                if let distance = routes[0]["legs"][0]["distance"].dictionary {
                    print(distance)
                    if let meters = distance["value"]?.int {
                        calculatedDistance = Double(meters) / 1000.0
                        print(calculatedDistance)
                    }
                }
                if let time = routes[0]["legs"][0]["duration"].dictionary{
                    if let sec = time["value"]?.int {
                        calculateMinute = Double(sec / 60)
                    }
                }
                for route in routes {
                    let routeOverviewPolyline = route["overview_polyline"].dictionary
                    let points = routeOverviewPolyline?["points"]?.stringValue
                    let path = GMSPath.init(fromEncodedPath: points!)
                    let bounds = GMSCoordinateBounds(path: path!)
                    let polyline = GMSPolyline(path: path)
                    polyline.strokeColor = .black
                    polyline.strokeWidth = 3.0
                    self.myView!.animate(with: GMSCameraUpdate.fit(bounds, withPadding: 30.0))
                    polyline.map = self.myView
                }
            }
        }
    }
    
    /// Fare calculation responds
  
    
    // Trip request responds
    func getTripRequestResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            print(response)
            if !response.isEmpty {
                if let success =  response["success"].bool {
                    if success {
                        animationLotti()
                        homeBack.isHidden = true
                        homeBackView.isHidden = true
                        if let requestId = response["generated_request_id"].int {
                            Helpers.setIntValueWithKey(requestId, key: Constants.TRIP_REQUEST_ID)
                        }
                    }else {
                        let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                        self.present(popup, animated: true, completion: nil)
                    }
                 
                }
            }
        }
    }

    func getDriverResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
            print(response)
            if !response.isEmpty {
                if let driverFastName = response["first_name"].string{
                    if let driverLastName = response["last_name"].string{
                        self.driverName.text = "\(driverFastName) \(driverLastName)"
                    }
                }
                if let chanel = response["channel_name"].string{
                    driverChanelName = chanel
//                    pubInit(Chanal: driverChanelName)
                    
                }
                if let driverPhoneNumbe = response["mobile_no"].string{
                    driverPhone = driverPhoneNumbe
                }
                if let driverId = response["driver_id"].int {
                    Helpers.setIntValueWithKey(driverId, key:  Constants.DRIVER_ID)
                   
                }
                if let Profileurl = response["profile_pic_url_alt"].string{
                    let urlString = Profileurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    let url = URL(string: urlString!)
                    self.driverImage.kf.setImage(with: url)
                }
     
                
                
                if let activeVahicale = response["active_vehicle"].dictionary{
                    if let model = activeVahicale["model"]?.string{
                      
                    }
                    if let carNumber = activeVahicale["license_plate"]?.string{
                      self.driverDesignation.text = carNumber
                    }
                    if let vehicalType = activeVahicale["category_type"]?.string{
                       
                    }
                    if let catId = activeVahicale["category_id"]?.int {
                        
                    }
                }
            }
        }
    }
    

    func getTripCancelResponse(_ data: [String: Any], statusCode: Int)  {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            if !response.isEmpty {
                if let success =  response["success"].bool {
                    if success {
                        ManageHomePageViewAgain()
                        removerDriverInfo()
                        showTop()
                        homeBack.isHidden = false
                        homeBackView.isHidden = false
                        let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                        self.present(popup, animated: true, completion: nil)
                    }else {
                        let popup = CustomAlertPopup.customOkayPopup("", msgBody: response["message"].string!)
                        self.present(popup, animated: true, completion: nil)
                        
                    }
                }
            }
        }
    }
    
    //stage manage

 public func manageStage(){
        
        let stageInfo = Helpers.getStringValueForKey(Constants.STAGE_DATA)
        userStage = JSON.init(parseJSON: stageInfo)
    
    
        if let Data = userStage["data"].dictionary{
            if let stageData = Data["stage_data"]?.dictionary{
                
                
                if let tripId = stageData["request_id"]?.int {
                    Helpers.setIntValueWithKey(tripId, key: Constants.TRIP_ID)
                }
                if let passengerStage = stageData["passenger_stage"]?.int{
                    
                }
                if let driverStage = stageData ["driver_stage"]?.int{
                    self.StaticDriverStage = driverStage
                    print(StaticDriverStage)
                    if driverStage == 1 {
                        animationLotti()
                    }else if driverStage == 2{
                        stopAnimationLotti()
                        self.homeBackView.isHidden = true
                        self.homeBack.isHidden = true
                        isStageData = true
                        addDriveInfo()
                    }else{
                        navigateToFareAndRating()
                    }
                }
                if let pessengerObj = stageData["passenger_object"]?.dictionary{
                    if let stage1 = pessengerObj["stage_1"]?.dictionary {
                        if let dataRecive = stage1["data_received"]?.dictionary{
                            if let destinationName = dataRecive["dest_address"]?.string{
                                (parent as? DriverDetailsViewController)?.passengerDropLocation.text = "destinationName"
                            }
                            if let piclocation = dataRecive["location_name"]?.string{
                                (parent as? DriverDetailsViewController)?.passengerPicupLocation.text = piclocation
                            }
                            if let estimateFare = dataRecive["estimated_fare"]?.string{
                                (parent as? DriverDetailsViewController)?.passengerTotalFare.text = estimateFare
                            }
                            if StaticDriverStage != 101 && StaticDriverStage != 0{
                                if let destLat = dataRecive["dest_latitude"]?.string{
                                    if let destLng = dataRecive["dest_longitude"]?.string{
                                        self.origin = "\(destLat),\(destLng)"
                                        
                                    }
                                }
                                if let picLat = dataRecive["latitude"]?.string{
                                    if let picLng = dataRecive["longitude"]?.string{
                                        self.destination = "\(picLat),\(picLng)"
                                        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                            self.directionService?.getRoutes(self.origin, destination: self.destination)
                                        }
                                    }
                                }
                            }
                            
                            
                        }
                    }
                    if let stage2_2 = pessengerObj["stage_2_2"]?.dictionary{
                        if let dataDriverDetails = stage2_2["data_driver_details"]?.dictionary{
                            if let code = dataDriverDetails["code"]?.int{
                                if code == Constants.SUCCESS_STATUS_CODE {
                                    if let driverFastName = dataDriverDetails["first_name"]?.string{
                                        if let driverLastName = dataDriverDetails["last_name"]?.string{
                                            (parent as? DriverDetailsViewController)?.driverName.text = "\(driverFastName) \(driverLastName)"
                                        }
                                    }
                                    if let chanel = dataDriverDetails["channel_name"]?.string{
                                       
                                    }
                                    if let driverPhoneNumbe = dataDriverDetails["mobile_no"]?.string{
                                        driverPhone = driverPhoneNumbe
                                    }
                                    if let driverId = dataDriverDetails["driver_id"]?.int {
                                        Helpers.setStringValueWithKey(String(driverId), key: Constants.DRIVER_ID)
                                    }
                                    if let Profileurl = dataDriverDetails["profile_pic_url_alt"]?.string{
                                        let urlString = Profileurl.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                                        let url = URL(string: urlString!)
                                        (parent as? DriverDetailsViewController)?.driverImage.kf.setImage(with: url)
                                    }
                                    if let rating = dataDriverDetails["driver_rating"]?.int{
                                        (parent as? DriverDetailsViewController)?.driverRating.text = String(rating)
                                    }
                                    if let totalTrip = dataDriverDetails["driver_trip_count"]?.int{
                                        (parent as? DriverDetailsViewController)?.totalTrip.text = "Total trip : \(totalTrip)"
                                    }
                                    if let activeVahicale = dataDriverDetails["active_vehicle"]?.dictionary{
                                        if let model = activeVahicale["model"]?.string{
                                            (parent as? DriverDetailsViewController)?.carName.text = model
                                        }
                                        if let carNumber = activeVahicale["license_plate"]?.string{
                                            (parent as? DriverDetailsViewController)?.carNumber.text = carNumber
                                        }
                                        if let categoris = activeVahicale["category_id"]?.int {
                                        }
                                        if let vehicalType = activeVahicale["category_type"]?.string{
                                            (parent as? DriverDetailsViewController)?.driverCarType .text = vehicalType
                                        }
                                    }
                                    
                                }
                            }
                        }
                    }
              
                }
                
            }
            
        }
        
    }
    func navigationToEditProfileController() {
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let editProfileController = mainStoryboard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileController
        self.navigationController?.pushViewController(editProfileController!, animated: true)
    }
    
    
    func profileInfoSetup(){
        let userinfo = Helpers.getStringValueForKey(Constants.PROFILE_INFO)
        let userInfoJSON = JSON.init(parseJSON: userinfo)
        print(userInfoJSON)
        if !userInfoJSON.isEmpty {
            if let victimeEmergencyContactNumber = userInfoJSON["emg_mobile_no"].string{
                self.victimeEContact.text = "Emergency Contact: +88 \(victimeEmergencyContactNumber)"
            }
            if let victimeAddress = userInfoJSON["address"].string {
                self.victimeAddress.text = "Address:  \(victimeAddress)"
            }
            if let profile_picture_url = userInfoJSON["profile_pic_url"].string {
                if profile_picture_url != "" {
                    let urlString = profile_picture_url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    let url = URL(string: urlString!)
                    victimeImage.kf.indicatorType = .activity
                    victimeImage.kf.setImage(with: url, placeholder: UIImage(named: "placeImage"))
                }else{
                    alart.showAlert("Attention", subTitle: "Please upload your profile Photo", style: .warning, buttonTitle: "Upload") { (true) in
                        isProfilePicAvailAble = true
                        self.navigationToEditProfileController()
                    }
                }

            }
            else {
                victimeImage.image = UIImage(named: "placeImage")
            }
            
            var fullName = ""
            if let phoneNumber = userInfoJSON["mobile_no"].string {
                Helpers.setStringValueWithKey(phoneNumber, key: Constants.USER_PHONE)
                let chanel = "chnl_dc_\(phoneNumber)"
                 pubInit(Chanal: chanel)
                
            }
            if let first_name = userInfoJSON["first_name"].string {
                fullName = first_name
            }
            if let last_name = userInfoJSON["last_name"].string {
                if fullName.count > 0 {
                    fullName = fullName + " " + last_name
                }
            }
            userName.text = fullName.uppercased()
            

        }
    }
    /// User profile respond
    
    func getProfileResponse(_ data: [String: Any], statusCode: Int) {
        if statusCode == Constants.STATUS_CODE_SUCCESS {
            let response = JSON(data)
            if !response.isEmpty {
                if loginTace == 0 {
                    if let stageData = response["stage_data"].dictionary {
                        if let driverStage = stageData["driver_stage"]?.int {
                            if driverStage != 101 && driverStage != 0 {
                                requestForStage()
                            }else {
                            }
                        }
                    }
                }else{
                }
                if let profileInfo = response.rawString() {
                   
                    Helpers.removeValue(Constants.PROFILE_INFO)
                    Helpers.setStringValueWithKey(profileInfo, key: Constants.PROFILE_INFO)
                     profileInfoSetup()
                }
            }
        }
        else {
            
            Helpers.removeValue(Constants.PROFILE_INFO)
            Helpers.removeValue(Constants.USER_ID)
            UIApplication.shared.keyWindow?.rootViewController = initViewController()
        }
    }
    // User Stage Responds
    func getUserStageDataResponse(_ data: [String: Any], statusCode: Int){
        if statusCode == Constants.SUCCESS_STATUS_CODE {
            let response = JSON(data)
          
            if !response.isEmpty {
                if let stageData = response.rawString() {
                    Helpers.removeValue(Constants.STAGE_DATA)
                    Helpers.setStringValueWithKey(stageData, key: Constants.STAGE_DATA)
                    manageStage()
                }
            }
        }
    }
    
    // fake car responds

}
