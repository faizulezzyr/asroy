//
//  MarkerInfoView.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 8/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
class MarkerInfoView: UIView {
    
   
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "infoView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}
