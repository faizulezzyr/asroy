//
//  File.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 4/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import SwiftyJSON
import CoreLocation
import GoogleMaps
import GoogleUtilities
extension HomeController: UITextViewDelegate {
    func setLogoConstraint(){
        anView.addSubview(logoImage)
        anView.bringSubviewToFront(logoImage)
       
        let heightConstraint = NSLayoutConstraint(item: logoImage, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
        let widthConstraint = NSLayoutConstraint(item: logoImage, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 100)
        let centerXConstraint = NSLayoutConstraint(item: logoImage, attribute: .centerX, relatedBy: .equal, toItem: anView, attribute: .centerX, multiplier: 1, constant: 0)
        let centerYConstraint = NSLayoutConstraint(item: logoImage, attribute: .centerY, relatedBy: .equal, toItem: anView, attribute: .centerY, multiplier: 1, constant: 0)
        NSLayoutConstraint.activate([heightConstraint, widthConstraint, centerXConstraint, centerYConstraint])
        
        
        anView.updateConstraints()
    }
    func readJson(fileName: String) {
        do {
            if let file = Bundle.main.url(forResource: fileName, withExtension: "json") {
                let data = try Data(contentsOf: file)
                
                let json = try JSONSerialization.jsonObject(with: data, options: [])
                if let object = json as? [String: Any] {
                    
                    let jData = JSON(data)
                    DispatchQueue.main.async {
                        self.objectDivisionJSON = jData
                
                        self.SearchISZoneContain()
                    }
                } else if let object = json as? [Any] {
                    // json is an array
                    
                } else {
                    print("JSON is invalid")
                }
            } else {
                print("no file")
            }
        } catch {
            print(error.localizedDescription)
        }
    }
  
    func SearchISZoneContain(){
        let dispatchGroup = DispatchGroup()
        dispatchGroup.enter()
        var allzone: JSON = [:]
        if let allZone = self.objectDivisionJSON["geojson"].arrayObject {
            allzone = JSON(allZone)
            
        }
        DispatchQueue.main.async {
            let whichLocation = "dhaka"
            var i = 0
            while i < allzone.count {
                if whichLocation == allzone[i]["zone"].string{
        
                    if let zone = allzone[i]["coordinate"]["features"].arrayObject{
                        let GJson = JSON(zone)
                        
                        var dict = GJson[0]["geometry"].dictionaryValue
                        let cordinatne = dict["coordinates"]?.arrayValue
                        
                        let secondCordinate = cordinatne?[0]
                        self.path.removeAllCoordinates()
                        for i in 0..<secondCordinate!.count{
                            self.path.add(CLLocationCoordinate2D(latitude: secondCordinate![i][1].double!, longitude: secondCordinate![i][0].double!))
                        }
                    }
                    if let subzoneGeo = allzone[i]["geojson"].array{
                        var i = 0
                        while i < subzoneGeo.count {
                            let subzoneString = subzoneGeo[i]["sub_zone"].string
                         
                            if let subzone = subzoneGeo[i]["coordinate"]["features"].arrayObject{
                                let subGeoJson = JSON(subzone)
                                for i in 0..<subGeoJson.count {
                                    var dict = subGeoJson[i]["geometry"].dictionaryValue
                                    if  let type = dict["type"]?.string {
                                        if type == "Polygon"{
                                            let cordinate = dict["coordinates"]?.arrayValue
                                            let secondCordinate = cordinate?[0]
                                            self.subZonePath.removeAllCoordinates()
                                            for i in 0..<secondCordinate!.count {
                                                self.subZonePath.add(CLLocationCoordinate2D(latitude: secondCordinate![i][1].double!, longitude: secondCordinate![i][0].double!))
                                            }
                                        }else {
                                            self.subZonePath.removeAllCoordinates()
                                            let cordinate = dict["coordinates"]?.array
                                            for i in 0..<cordinate!.count{
                                                let secondcordinate = cordinate![i]
                                                for i in 0..<secondcordinate.count{
                                                    let thirdCordinate = secondcordinate[i]
                                                    for i in 0..<thirdCordinate.count{
                                                        self.subZonePath.add(CLLocationCoordinate2D(latitude: thirdCordinate[i][1].double!, longitude: thirdCordinate[i][0].double!))
                                                        
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            let isServiceForSubzone = self.fanchingSubzone(CLLocationCoordinate2D(latitude: myCurrentLocation!.latitude, longitude: myCurrentLocation!.longitude))
                            print(isServiceForSubzone)
                            if isServiceForSubzone == true {
                                self.pickUpSubzone =  subzoneString!
                                print(subzoneString!)
                                i = subzoneGeo.count
                            }else {
                                i = i + 1
                            }
                            
                        }
                    }
                    
                    i = allzone.count
                }else {
                    i = i + 2
                }
            }
        }
        dispatchGroup.leave()
        
        dispatchGroup.enter()
        DispatchQueue.main.async {
            let whichLocation = "dhaka"
            var i = 1
            while i < allzone.count {

                if whichLocation == allzone[i]["zone"].string{
                    
                    if let zone = allzone[i]["coordinate"]["features"].arrayObject{
                        let GJson = JSON(zone)
                        var dict = GJson[0]["geometry"].dictionaryValue
                        let cordinatne = dict["coordinates"]?.arrayValue
                        let secondCordinate = cordinatne?[0]
                        self.path.removeAllCoordinates()
                        for i in 0..<secondCordinate!.count{
                            self.path.add(CLLocationCoordinate2D(latitude: secondCordinate![i][1].double!, longitude: secondCordinate![i][0].double!))
                        }
                    }
                    if let subzoneGeo = allzone[i]["geojson"].array{
                        var i = 0
                        while i < subzoneGeo.count {
                            let subzoneString = subzoneGeo[i]["sub_zone"].string
                       
                            if let subzone = subzoneGeo[i]["coordinate"]["features"].arrayObject{
                                let subGeoJson = JSON(subzone)
                                
                                for i in 0..<subGeoJson.count {
                                    var dict = subGeoJson[i]["geometry"].dictionaryValue
                                    if  let type = dict["type"]?.string {
                                        if type == "Polygon"{
                                            let cordinate = dict["coordinates"]?.arrayValue
                                            let secondCordinate = cordinate?[0]
                                            self.subZonePath.removeAllCoordinates()
                                            for i in 0..<secondCordinate!.count {
                                                self.subZonePath.add(CLLocationCoordinate2D(latitude: secondCordinate![i][1].double!, longitude: secondCordinate![i][0].double!))
                                            }
                                        }else {
                                            self.subZonePath.removeAllCoordinates()
                                            let cordinate = dict["coordinates"]?.array
                                            for i in 0..<cordinate!.count{
                                                var secondcordinate = cordinate![i]
                                                for i in 0..<secondcordinate.count{
                                                    var thirdCordinate = secondcordinate[i]
                                                    for i in 0..<thirdCordinate.count{
                                                        
                                                        self.subZonePath.add(CLLocationCoordinate2D(latitude: thirdCordinate[i][1].double!, longitude: thirdCordinate[i][0].double!))
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            
                            let idlPosition = CLLocationCoordinate2D(latitude: self.markLet!, longitude: self.markLng!)
                            let isServiceForSubzone = self.fanchingSubzone(CLLocationCoordinate2D(latitude: myCurrentLocation!.latitude, longitude: myCurrentLocation!.longitude))
                                 print(isServiceForSubzone)
                            if isServiceForSubzone == true{
                                self.pickUpSubzone =  subzoneString!
                                print(subzoneString!)
                                i = subzoneGeo.count
                            }else {
                              
                                i = i + 1
                            }
                        }
                        
                    }
                    
                    i = allzone.count
                }else {
                    i = i + 2
                }
            }
            dispatchGroup.leave()   // <<----
        }
        dispatchGroup.notify(queue: .main) {
       
            let idlPosition = CLLocationCoordinate2D(latitude: self.markLet!, longitude: self.markLng!)
            let isService = self.isWithin(idlPosition)

           // print(isService)
            if isService == true{
                
            }
        }
        
    }
    func isWithin(_ point: CLLocationCoordinate2D) -> Bool {
        return GMSGeometryContainsLocation(point, path, true)
    }
    func fanchingSubzone(_ point: CLLocationCoordinate2D) -> Bool{
        return GMSGeometryContainsLocation(point, subZonePath, true)
        
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = ""
            textView.textColor = UIColor.black
        }
    }
//    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView.text.isEmpty {
//            textView.text = "Enter your Details here..."
//            textView.textColor = UIColor.lightGray
//        }
//    }
    
    func showCancelAlart() {
        
        let alertController = UIAlertController(title: "", message: "Trip cancel", preferredStyle: .actionSheet)
        
        let saveAction = UIAlertAction(title: "Cancel", style: .destructive, handler: {
            alert -> Void in
            
            
        })
        if cancelRequestLabel == 1 {
            let cancelAction = UIAlertAction(title: "Keep searching", style: .cancel, handler: {
                (action : UIAlertAction!) -> Void in
            })
            
            alertController.addAction(cancelAction)
        }else if cancelRequestLabel == 2 {
            let cancelAction = UIAlertAction(title: "Keep Riding", style: .cancel, handler: {
                (action : UIAlertAction!) -> Void in
            })
            alertController.addAction(cancelAction)
        }
        alertController.addAction(saveAction)
        
        self.present(alertController, animated: true, completion: nil)
    }

  
    
}
