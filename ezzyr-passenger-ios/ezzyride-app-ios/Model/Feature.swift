//
//  Feature.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Feature {
    
    var type: String
    var geometry: Geometry
    var properties: Property
    
    init() {
        type = ""
        geometry = Geometry()
        properties = Property()
    }
    
    init(type: String, geometry: Geometry, properties: Property) {
        self.type = type
        self.geometry = geometry
        self.properties = properties
    }
}
