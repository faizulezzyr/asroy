//
//  Features.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Features {
    
    var features: [Feature]
    
    init() {
        features = [Feature]()
    }
    
    init(features: [Feature]) {
        self.features = features
    }
}


