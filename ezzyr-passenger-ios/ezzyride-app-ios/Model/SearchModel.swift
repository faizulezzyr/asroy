//
//  SearchModel.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 2/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
struct SearchPlaceMode {
    let results: [Result]?

}

struct Result {
    let geometry: Geometrys?
    let name: String?
    let reference: String?
    let icon: String?
    let id, placeID, formattedAddress: String?
    let rating: Int?
//    let photos: [Photos]?
//    let types: [String]?
}

struct Geometrys {
    let viewport: Viewport?
    let location: Location?
}

struct Location{
    let lat, lng: Double?
}

struct Viewport {
    let northeast, southwest: Location?
}

//struct Photos {
//    let photoReference: String?
//    let height: Int?
//    let width: Int?
//}
