//
//  DropupLocationModel.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 12/7/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation


class DropupLocationModel {
    
    var latitude: String
    var longitude: String
    var altitude: String
    var locationName: String
    var dropZone: String
    var subZone: String
    var division: String
    var upZila: String
    var palceName : String
    var isIncity: Bool
    
    
    init() {
        latitude = ""
        longitude = ""
        altitude = ""
        locationName = ""
        dropZone = ""
        subZone = ""
        division = ""
        upZila = ""
        palceName = ""
        isIncity = false
    }
    
    init(latitude: String, longitude: String, altitude: String, locationName: String, dropZone: String,Subzone: String, Division: String,Upzila: String,PlaceName: String, IsIncity: Bool) {
        self.latitude = latitude
        self.longitude = longitude
        self.altitude = altitude
        self.locationName = locationName
        self.dropZone = dropZone
        self.subZone = Subzone
        self.division = Division
        self.isIncity = IsIncity
        self.upZila = Upzila
         self.palceName = PlaceName
    }
}
