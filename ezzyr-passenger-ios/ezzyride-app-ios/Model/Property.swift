//
//  Property.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Property {
    
    var style: String
    
    init() {
        style = ""
    }
    
    init(style: String) {
        self.style = style
    }
}
