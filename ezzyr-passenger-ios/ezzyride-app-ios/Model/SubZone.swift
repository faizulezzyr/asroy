//
//  SubZone.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class SubZone {
    
    var sub_zone: String
    var coordinate: BaseCoordinate
    
    init() {
        sub_zone = ""
        coordinate = BaseCoordinate()
    }
    
    init(sub_zone: String, coordinate: BaseCoordinate) {
        self.sub_zone = sub_zone
        self.coordinate = coordinate
    }
}
