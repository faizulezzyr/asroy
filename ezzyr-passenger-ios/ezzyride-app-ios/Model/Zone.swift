//
//  Zone.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Zone {
    
    var zone: String
    var coordinate: BaseCoordinate
    var cityCount: Int
    var subZones: [SubZone]
    
    init() {
        zone = ""
        coordinate = BaseCoordinate()
        cityCount = 0
        subZones = [SubZone]()
    }
    
    init(zone: String, coordinate: BaseCoordinate, cityCount: Int, subZones: [SubZone]) {
        self.zone = zone
        self.coordinate = coordinate
        self.cityCount = cityCount
        self.subZones = subZones
    }
}




