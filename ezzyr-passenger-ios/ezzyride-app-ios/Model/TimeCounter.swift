//
//  TimeCounter.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 5/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
enum Week: Int {
    case one = 7, two = 14, three = 21
}

enum CountdownResponse {
    case isFinished
    case result(time: String)
}

struct ProductListing {
    // Customize this if you want to change timeRemaining's format
    // It automatically take care of singular vs. plural, i.e. 1 hr and 2 hrs
    private static let dateComponentFormatter: DateComponentsFormatter = {
        var formatter = DateComponentsFormatter()
        formatter.allowedUnits = [.day, .hour, .minute, .second]
        formatter.unitsStyle = .short
        return formatter
    }()
    
    var listingDate: Date
    var duration: Week
    var expirationDate: Date {
        return Calendar.current.date(byAdding: .day, value: duration.rawValue, to: listingDate)!
    }
    
    var timeRemaining: CountdownResponse {
        let now = Date()
        
        if expirationDate <= now {
            return .isFinished
        } else {
            let timeRemaining = ProductListing.dateComponentFormatter.string(from: now, to: expirationDate)!
            return .result(time: timeRemaining)
        }
    }
}
    
