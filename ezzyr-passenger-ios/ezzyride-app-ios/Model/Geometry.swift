//
//  Geometry.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Geometry {
    
    var type: String
    var coordinates: [Coordinates]
    
    init() {
        type = ""
        coordinates = [Coordinates]()
    }
    
    init(type: String, coordinates: [Coordinates]) {
        self.type = type
        self.coordinates = coordinates
    }
}


