//
//  Division.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 10/27/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation

class Division {
    
    var division: String
    var cityCount: Int
    var zones: [Zone]
    
    init() {
        division = ""
        cityCount = 0
        zones = [Zone]()
    }
    
    init(division: String, cityCount: Int, zones: [Zone]) {
        self.division = division
        self.cityCount = cityCount
        self.zones = zones
    }
}


