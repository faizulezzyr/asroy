//
//  BaseViewController.swift
//  BarTest
//
//  Created by faizul hasan on 3/24/17.
//  Copyright © 2017 faizul hasan. All rights reserved.
//

import UIKit


 var senderTrack = 0

 class BaseController: UIViewController {
      var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
 
    func hideView(sender: UIButton) {

         sender.tag = 0;
        senderTrack = 0
         let viewMenuBack : UIView = view.subviews.last!
         UIView.animate(withDuration: 0.3, animations: { () -> Void in
            var frameMenu : CGRect = viewMenuBack.frame
            frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
            viewMenuBack.frame = frameMenu
            viewMenuBack.layoutIfNeeded()
            viewMenuBack.backgroundColor = UIColor.clear
          
           // sender.isEnabled = true
        }, completion: { (finished) -> Void in
             viewMenuBack.removeFromSuperview()
        })
        print(sender.tag)
       return
    }

    func showView(sender: UIButton){
              let menuVC : SideBarController = self.storyboard!.instantiateViewController(withIdentifier: "MenuViewController") as! SideBarController
        if senderTrack == 0 {
            self.view.addSubview(menuVC.view)
            self.addChild(menuVC)
            menuVC.view.layoutIfNeeded()
            menuVC.view.frame=CGRect(x: 0 - UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                menuVC.view.frame=CGRect(x: 0, y: 0, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height);
//                sender.isEnabled = false
                sender.tag = 10
                senderTrack = 10
                print(sender.tag)
            }, completion: { (finished) -> Void in
                
            })
        }else {
            let viewMenuBack : UIView = view.subviews.last!
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                var frameMenu : CGRect = viewMenuBack.frame
                frameMenu.origin.x = -1 * UIScreen.main.bounds.size.width
                viewMenuBack.frame = frameMenu
                viewMenuBack.layoutIfNeeded()
                viewMenuBack.backgroundColor = UIColor.clear
                senderTrack = 0
               //sender.isEnabled = true
            }, completion: { (finished) -> Void in
                viewMenuBack.removeFromSuperview()
            })
             return
        }
  

    }
    @objc func onSlideMenuButtonPressed(_ sender : UIButton){
//        if (senderTrack == 10)
//        {
//        hideView(sender: sender)
//        }else {
            showView(sender: sender)
       // }
    }
    
    func displayContentController(content: UIViewController) {
        addChild(content)
        self.view.addSubview(content.view)
        content.didMove(toParent: self)
    }

}

