//
//  AmbulanceService.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 20/2/19.
//  Copyright © 2019 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import Alamofire
class AmbulanceService : NSObject {
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    
    func doAmbulanceList(_ params: Parameters){
        let url = Urls.BASE_URL + Urls.AMBULANCE_LIST
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
        
    }
}
