//
//  AuthenticationService.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 11/9/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Foundation
import Alamofire

class AuthenticationService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func doValidatePhoneNumber(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.PHONE_NUMBER_VARIFICATION_URL
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
    }

    
    func doLogin(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.LOGIN_URL
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
    }
    
    func doValidateCode(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.VALIDATE_CODE
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.SECOND, withHeader: false)
    }

    
    func doRegisterUser(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.USER_REGISTER_URL
        print(url)
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
    }
    
    func doForgotPasswordOtpRequest(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.FORGOT_PASSWORD_OTP_REQUEST
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.THIRD, withHeader: false)
    }
    func doResetPassword(_ params: Parameters) {
        let url = Urls.BASE_URL + Urls.RESET_PASSWORD
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: false)
    }
    
    func doUpdateToken(_ params: Parameters){
        let url = Urls.BASE_URL + Urls.UPDETE_TOKEN
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIFTH, withHeader: true)
       
    }
}




