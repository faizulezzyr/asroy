//
//  DirectionService.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 27/11/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class DirectionService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    // origin = Name or latlon (Mohakhali or 23.777628,90.405449) (latlon separated by coma)
    // destination = Name or latlon (Gulshan or 23.797911,90.414391) (latlon separated by coma)
    func getRoutes(_ origin: String, destination: String) {
        
        let url = Urls.GOOGLE_MAP_DIRECTION_API_BASE_URL + Urls.ORIGIN_KEY + origin + Urls.DESTINATION_KEY + destination + Urls.API_KEY + Constants.GOOGLE_MAP_API_KEY
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.THIRD, withHeader: false)
    }
}











