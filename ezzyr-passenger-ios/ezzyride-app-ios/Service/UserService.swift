//
//  UserService.swift
//  ezzyride-app-ios
//
//  Created by Riajur Rahman on 11/14/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class UserService: NSObject {
    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    func getProfile(_ member_id: Int) {
        let url = Urls.BASE_URL + Urls.USER_PROFILE_URL + String(member_id)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
  
    }
    
    func getProfile(_ member_id: Int, from: String) {
        let url = Urls.BASE_URL + Urls.USER_PROFILE_URL + String(member_id)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.SIXTH, withHeader: true)
    }
    
    func editProfile(_ params: Parameters) {
        let member_id = Helpers.getIntValueForKey(Constants.MEMBER_ID)
        let url = Urls.BASE_URL + Urls.EDIT_PROFILE_URL + String(member_id)
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: true)
    }
    
    func uploadProfileImage(_ params: Parameters, imageData: Data) {
        let url = Urls.BASE_URL + Urls.UPLOAD_PROFILE_IMAGE_URL
        apiCommunicatorHelper?.uploadPhoto(url, imageData: imageData, params: params, methodTag: MethodTags.SECOND, withHeader: true)
    }
    func changePassword(_ params: Parameters) {
 
        let url = Urls.BASE_URL + Urls.CHANGE_PASSWORD
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.THIRD, withHeader: true)
    }
    func getUserTripHistory(pageNum: Int, _ member_id: Int ) {
        let url = Urls.BASE_URL + Urls.TRIP_HISTORY + String(member_id) + "?page=\(pageNum)" + Urls.PER_PAGE
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func getUserTripHistoryDetails(TripId: Int, _ member_id: Int ) {
        let url = Urls.BASE_URL + Urls.TRIP_DETAILS + "trip/\(TripId)" + "/passenger/" + String(member_id) 
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: true)
    }
    func postPromoCode(_ params: Parameters){
        let url = Urls.BASE_URL + Urls.PROMO_CODE
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.FIRST, withHeader: true)
    }
    
    func getPromoCode(){
        let user_Id = Helpers.getIntValueForKey(Constants.USER_ID)
        let url = Urls.BASE_URL + Urls.GET_PROMO + String(user_Id)
       
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.SECOND, withHeader: true)
    }
    func getNotification(){
        let url = Urls.BASE_URL + Urls.MESSAGE
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FIRST, withHeader: false)
    }
}





