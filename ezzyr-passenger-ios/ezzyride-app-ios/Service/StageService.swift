//
//  StageService.swift
//  ezzyride-app-ios
//
//  Created by Innovadeaus on 23/12/18.
//  Copyright © 2018 Innovadeus Pvt. Ltd. All rights reserved.
//

import UIKit
import Alamofire
class StageService: NSObject {

    
    var apiCommunicatorHelper: APICommunicator?
    
    var view: UIView?
    
    init(_ view: UIView, communicator: APICommunicator) {
        self.view = view
        self.apiCommunicatorHelper = communicator
    }
    
    
    func doStageRequest(_ params: Parameters){
        let url = Urls.BASE_URL + Urls.STAGE_DATA
        print(url)
        apiCommunicatorHelper?.getDataByPOST(url, params: params, methodTag: MethodTags.ELEVENTH, withHeader: true)
    }
    func doDriverDetailsRequest(id: Int){
        let url = Urls.BASE_URL + Urls.ACCEPT_BY_DRIVER + String(id)
        print(url)
        apiCommunicatorHelper?.getDataByGET(url, methodTag: MethodTags.FOURTH, withHeader: true)
    }
}
